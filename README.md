# Todo list with React and Typescript

## Getting Started

- `git clone https://gitlab.com/lednhatkhanh/react-ts-todo`.
- `cd react-ts-todo`.
- Run `npm install` to install app dependencies.
- Run `npm run start-server` to start the fake server, server is served at [http://localhost:8000/todos](http://localhost:8000/todos).
- Run `npm start` and navigate to [http://localhost:3000](http://localhost:3000) to see the app.
- All data will be saved to `db.json`.

## Resources

- [Typescript handbook](https://www.typescriptlang.org/docs/handbook/basic-types.html).
- [React tutorial](https://reactjs.org/tutorial/tutorial.html).
- [React docs](https://reactjs.org/docs/getting-started.html).
- [Microsoft's tutorial](https://github.com/Microsoft/TypeScript-React-Starter#creating-a-component).
