import React from "react";
import {
  Grid,
  withStyles,
  WithStyles,
  StyleRulesCallback,
  Typography,
  TextField,
  List,
} from "@material-ui/core";

import { Todo } from "../models/todo.model";
import { TodoItem } from "./todo-item";

interface Props extends WithStyles<Classes> {}

interface State {
  readonly todos: Todo[];
  readonly title: string;
}

const BASE_URL = "http://localhost:8000/todos";

class AppView extends React.Component<Props, State> {
  state: State = {
    todos: [],
    title: "",
  };

  async componentDidMount(): Promise<void> {
    const rawResult = await fetch(BASE_URL);
    const todos = (await rawResult.json()) as Todo[];
    this.setState({
      todos,
    });
  }

  render(): React.ReactNode {
    const { classes } = this.props;
    const { todos, title } = this.state;

    return (
      <Grid container justify="center">
        <Grid item xs={11} sm={8} lg={6} className={classes.main}>
          <Typography variant="h3" align="center" gutterBottom>
            Todo List
          </Typography>
          <TextField
            fullWidth
            variant="outlined"
            placeholder="Enter title here"
            label="Title"
            value={title}
            onChange={this.onTitleChange}
            onKeyPress={this.onKeyPress}
          />
          <List>
            {todos.map(todo => (
              <TodoItem
                todo={todo}
                key={todo.id}
                onToggle={this.onToggleTodo}
                onDelete={this.onDeleteTodo}
              />
            ))}
          </List>
        </Grid>
      </Grid>
    );
  }

  onTitleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({
      title: event.target.value,
    });
  };

  onKeyPress = async (event: React.KeyboardEvent<HTMLInputElement>): Promise<void> => {
    if (event.key === "Enter") {
      event.preventDefault();

      this.onAddTodo();
    }
  };

  onAddTodo = async (): Promise<void> => {
    try {
      const { title } = this.state;
      const response = await fetch(BASE_URL, {
        method: "POST",
        body: JSON.stringify({ title, completed: false }),
        headers: {
          "Content-Type": "application/json",
        },
      });
      const newTodo = (await response.json()) as Todo;
      this.setState(({ todos }) => ({ todos: [...todos, newTodo], title: "" }));
    } catch (error) {
      // tslint:disable-next-line:no-console
      console.log(error);
    }
  };

  onToggleTodo = async (todo: Todo): Promise<void> => {
    try {
      const response = await fetch(`${BASE_URL}/${todo.id}`, {
        method: "PATCH",
        body: JSON.stringify({ completed: !todo.completed }),
        headers: {
          "Content-Type": "application/json",
        },
      });
      const updatedTodo = (await response.json()) as Todo;
      this.setState(({ todos }) => ({
        todos: todos.map(todoItem =>
          todoItem.id === updatedTodo.id
            ? { ...todoItem, completed: updatedTodo.completed }
            : todoItem,
        ),
      }));
    } catch (error) {
      // tslint:disable-next-line:no-console
      console.log(error);
    }
  };

  onDeleteTodo = async (todo: Todo): Promise<void> => {
    try {
      await fetch(`${BASE_URL}/${todo.id}`, {
        method: "DELETE",
      });

      this.setState(({ todos }) => ({
        todos: todos.filter(todoItem => todoItem.id !== todo.id),
      }));
    } catch (error) {
      // tslint:disable-next-line:no-console
      console.log(error);
    }
  };
}

type Classes = "main";
const styles: StyleRulesCallback<Classes> = ({ spacing }) => ({
  main: {
    marginTop: spacing.unit * 10,
  },
});

export const App = withStyles(styles)(AppView);
