export function sum(a: number, b: number): number {
  return a + b;
}

export default function minus(a: number, b: number): number {
  return a - b;
}

export function multiple(a: number, b: number): number {
  return a * b;
}
