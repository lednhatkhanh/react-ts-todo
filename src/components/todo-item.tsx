import React from "react";
import {
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  StyleRulesCallback,
  WithStyles,
  withStyles,
  Dialog,
  DialogTitle,
  DialogActions,
  Button,
} from "@material-ui/core";

import DeleteIcon from "@material-ui/icons/Delete";

import { Todo } from "../models/todo.model";

interface Props extends WithStyles<Classes> {
  todo: Todo;
  onToggle: (todo: Todo) => void;
  onDelete: (todo: Todo) => void;
}

interface State {
  readonly openDialog: boolean;
}

class TodoItemView extends React.PureComponent<Props, State> {
  state: State = {
    openDialog: false,
  };

  render(): React.ReactNode {
    const { todo, classes } = this.props;
    const { openDialog } = this.state;
    const textClassName = [todo.completed ? classes.completed : undefined].join(" ").trim();

    return (
      <>
        <ListItem
          classes={{
            container: classes.container,
          }}
          key={todo.id}
          button
          onClick={this.onClick}
        >
          <ListItemText classes={{ root: textClassName }}>{todo.title}</ListItemText>
          <ListItemSecondaryAction className={classes.deleteButton}>
            <IconButton aria-label="Delete" onClick={this.onDeleteButtonClick}>
              <DeleteIcon color="error" />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
        <Dialog open={openDialog} onClose={this.onCloseDialog}>
          <DialogTitle>Do you want to remove this todo?</DialogTitle>
          <DialogActions>
            <Button onClick={this.onCloseDialog}>No</Button>
            <Button onClick={this.onConfirmDelete} color="secondary">
              Remove
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }

  onClick = (): void => {
    const { todo, onToggle } = this.props;
    onToggle(todo);
  };

  onDeleteButtonClick = (): void => {
    this.setState({
      openDialog: true,
    });
  };

  onCloseDialog = (): void => {
    this.setState({
      openDialog: false,
    });
  };

  onConfirmDelete = (): void => {
    const { onDelete, todo } = this.props;
    onDelete(todo);
    this.onCloseDialog();
  };
}

type Classes = "container" | "completed" | "deleteButton";
const styles: StyleRulesCallback<Classes> = () => ({
  deleteButton: {
    visibility: "hidden",
    opacity: 0,
    transition: "all ease-in-out .25s",
  },
  container: {
    "&:hover $deleteButton": {
      visibility: "visible",
      opacity: 1,
    },
  },
  completed: {
    textDecoration: "line-through",
  },
});

export const TodoItem = withStyles(styles)(TodoItemView);
