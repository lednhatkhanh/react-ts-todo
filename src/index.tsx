import React from "react";
import ReactDOM from "react-dom";
import { MuiThemeProvider, createMuiTheme, CssBaseline } from "@material-ui/core";

import blue from "@material-ui/core/colors/blue";
import pink from "@material-ui/core/colors/pink";

import { App } from "./components/app";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: blue[300],
      main: blue[500],
      dark: blue[700],
    },
    secondary: {
      light: pink[300],
      main: pink[500],
      dark: pink[700],
    },
  },
  typography: {
    useNextVariants: true,
  },
});

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <CssBaseline />
    <App />
  </MuiThemeProvider>,
  document.getElementById("root"),
);
